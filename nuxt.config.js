export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    htmlAttrs: {
      lang: 'pl_PL'
    },
    title: 'RADEC24 - Produkcja domów modułowych. FASTHOME.com.pl',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'description', content: 'RADEC24 - Produkcja domów modułowych. FASTHOME.com.pl' },
      { name: 'keywords', content: 'redec24, firma, domy modułowe, domy, modułowe, moduł, remontowo, budowlana, remonty, wykończenia, wykonczenia, wnętrz, inowroclaw, inowrocław, Inowroclaw, Inowrocław torun, toruń, Torun, Toruń, bydgoszcz, Bydgoszcz, kompleksowo, adaptacje, adaptacja, poddasze, poddaszy, wykonanie, sufity, napinane, Remonty i wykończenia wnętrz inowrocław, kompleksowo wykonane remonty, adaptacja poddaszy, sufity napinane,' },
      { name: 'og:description', content: 'RADEC24 - Produkcja domów modułowych. FASTHOME.com.pl' },
      { property: 'og:locale', content: 'pl_PL'},
      { property: 'og:title', content: 'Radec24 - Produkcja domów modułowych. FASTHOME.com.pl'},
      { property: 'og:type', content: 'website'},
      { property: 'og:image', content: 'https://radec24.vercel.app/thumbnail.jpg'}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'apple-touch-icon', href: '/maskable_icon.png' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    'vuesax/dist/vuesax.css',
    'boxicons/css/boxicons.css'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '@/plugins/vuesax',
    '@/plugins/vimg'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/robots',
    '@nuxtjs/pwa'
  ],

  pwa: {
    meta: {
      title: 'RADEC24 - Remonty i wykończenia wnętrz',
      author: 'MakotoPL'
    },
    manifest: {
      name: 'RADEC24 - Remonty i wykończenia wnętrz',
      short_name: 'RADEC24',
      lang: 'pl',
      background_color: '#e03a3a',
      theme_color : '#e03a3a',
      display: 'standalone',
      icons: [
        {
          src: '/maskable_icon.png',
          sizes: '512x512',
          purpose: 'any maskable',
        },
        {
          src: '/maskable_icon_192.png',
          sizes: '192x192',
          purpose: 'any maskable',
        },
      ],
      shortcuts: [
        {
          name: "test1",
          short_name: "Today",
          description: "View test1",
          url: "/",
          icons: [{ src: "/maskable_icon.png", sizes: "512x512" }]
        },
        {
          name: "test2",
          short_name: "Tomorrow",
          description: "View test2",
          url: "/kontakt",
          icons: [{ src: "/maskable_icon.png", sizes: "512x512" }]
        }
      ]
    }
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  }
}
